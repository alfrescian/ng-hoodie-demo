(function () {
    'use strict';

    angular.module('ng-hoodie-demo').controller('ProjectsController', function ($scope, ProjectService, $log, growl, $filter) {

        $scope.projects = [];
        ProjectService.getAll().then(function(projects){
            $scope.projects = projects;
        });
        
        $scope.remove = function (item) {
            ProjectService.remove(item);
        };

        $scope.save = function (title, description) {
            $scope.newProjectTitle = '';
            $scope.newProjectDescription = '';

            ProjectService.add(title, description);
        };

        ProjectService.onAdd(function(prj){
            $log.debug('on add: ' + prj.title);
            $scope.projects.push(prj);
        });

        ProjectService.onRemove(function (prj) {
            $log.debug('on remove:' + prj.title);
            $scope.projects = $filter('filter')($scope.projects, {
                id: prj.id
            }, function (actual, expected) {
                return !angular.equals(expected, actual);
            });
        });
    });

}());