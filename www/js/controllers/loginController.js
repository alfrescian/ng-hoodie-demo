(function () {
    'use strict';

    angular.module('ng-hoodie-demo').controller('LoginController', function ($scope, $location, hoodieAccount, $log, growl) {
        $scope.login = function () {
            hoodieAccount.signIn($scope.username, $scope.password).then(function (user) {
                growl.addSuccessMessage('Login successfull');
                $location.path("/").replace();
            }).catch(function () {
                growl.addErrorMessage('Login error');
            });
        };
    });

}());