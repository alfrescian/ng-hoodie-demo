(function () {
    'use strict';

    angular.module('ng-hoodie-demo').factory('ProjectService', function (hoodieStore) {
        return {
            onAdd: function (onAddCallback) {
                return hoodieStore.on('add:project', onAddCallback);
            },
            onRemove: function (onRemoveCallback) {
                return hoodieStore.on('remove:project', onRemoveCallback);
            },
            add: function (title, description) {
                hoodieStore.add('project', {
                    title: title,
                    description: description
                });
            },
            remove: function (project) {
                hoodieStore.remove('project', project.id);
            },
            getAll: function(){
                return hoodieStore.findAll('project');
            }
        };
    });
}());