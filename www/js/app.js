angular.module('ng-hoodie-demo', ['ui.bootstrap','hoodie','ngRoute','angular-growl','ngAnimate']);

angular.module('ng-hoodie-demo').config(function ($routeProvider, growlProvider) {
    "use strict";
    $routeProvider.when('/', {
        templateUrl: '/views/dashboard.html',
        controller: 'MainController'
    }).when('/login', {
        templateUrl: '/views/login.html',
        controller: 'LoginController',
        publicAccess: true
    }).when('/projects', {
        templateUrl: '/views/projects.html',
        controller: 'ProjectsController'
    }).otherwise({
        redirectTo: '/'
    });
    growlProvider.globalTimeToLive(2000);
});


angular.module('ng-hoodie-demo').run(function (hoodieAccount, $rootScope, $location, $log) {
    "use strict";
    $rootScope.signup = function () {
        hoodieAccount.signUp('jan', 'jan').then(function (user) {
            $log.debug('user signed up');
            $log.debug(user);
        });
    };

    $rootScope.logout = function () {
        hoodieAccount.signOut().then(function () {
            $location.path('/login').replace();
        });
    };

    hoodieAccount.on('signin', function (user) {
        $log.debug('user signed in');
        $log.debug(user);
    });

    hoodieAccount.on('signout', function (user) {
        $log.debug('user signed out');
        $log.debug(user);
    });
    //Authentication
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        var publicAccess = next.publicAccess || false;
        $log.log(next);
        $log.log(publicAccess);
        if (!publicAccess) {
            if (hoodieAccount.username === undefined) {
                $rootScope.nextRoute = $location.url();
                $location.path('/login').replace();
            }
        }

    });
});

